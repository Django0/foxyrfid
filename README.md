This is a hobby project for radio orienteering. This will upgrade the punchers from DB9 connector to an NFC based timing. Some of the code is developed based on other's code and hence, their respective licenses are applicable. In places where new code is developed and/or code that does not carry any license then the MIT License is applicable.

This readme is written such that anyone with little computer knowledge can perform this task or [when I am sixty-four](https://www.youtube.com/watch?v=HCTunqv1Xt4).

# To build locally on your desktop
Install [Arduino Sketch IDE](https://www.arduino.cc/en/software) version >= 1.8.13
### Required libraries
From Arduino IDE, go to Menu Sketch -> Include Library
- Search for "Adafruit PN532" with version >= 1.2.0 and choose the Adafruit provided library, also available here: [Adafruit-PN532](https://github.com/adafruit/Adafruit-PN532)
- Search for "TimerInterrupt" with version >= 1.1.1 and choose from Author Khoi Hoang, also available here: [TimerInterrupt](https://github.com/khoih-prog/TimerInterrupt)
- (Optional) Search for "Rtc by Makuna" with version >= 2.3.5 and choose from Author Makuna, also available here: [Rtc](https://github.com/Makuna/Rtc)

# To build directly in the cloud (GitLab)
Just edit your files and gitlab-ci will automatically create the binaries. See the output in pipeline/jobs and use avrdude to flash the Nano MCU.
## gitlab-ci with Alpine Linux Android SDK and Arduino-cli
The docker image: [django0/alpine-android-arduino-cli](https://hub.docker.com/repository/docker/django0/alpine-android-arduino-cli) is created based on [Preventis/docker-android-alpine](https://github.com/Preventis/docker-android-alpine). Alpine linux was chosen over ubuntu due to its size.
To reduce the burden on curling arduino setup files, the `arduino-cli` and the above libraries are installed via [`Dockerfile`](https://gitlab.com/Django0/foxyrfid/-/tree/rfid/ci/Dockerfile)

See [`.gitlab-ci.yml`](https://gitlab.com/Django0/foxyrfid/-/blob/rfid/.gitlab-ci.yml) as to how this is used.

# Flashing the MCU
To burn the firmware, you will need a programmer/debugger or simply the Arduino IDE or avrdude
## With Arduino IDE
### Programming the Arduino nano for orienteering
Just program the nano with this sketch: `programNanoNfcConnectedToFox.ino`

### Programming the Puncher ID
Just program the NFC transponder with this sketch: `programPuncherId.ino`. Open the serial port terminal and follow the instructions.

## With avrdude on Windows
Assuming you have downloaded the `artifacs.zip` containing the binaries from [gitlab jobs](https://gitlab.com/Django0/foxyrfid/-/jobs)
Either download the [avrdude](http://download.savannah.gnu.org/releases/avrdude/) or get it from [Arduino IDE](https://www.arduino.cc/en/software) installation. It is located 
```
C:\Users\<your login name>\AppData\Local\Arduino15\packages\arduino\tools\avrdude\6.3.0-arduino17/bin/avrdude -CC:\Users\<your login name>\AppData\Local\Arduino15\packages\arduino\tools\avrdude\6.3.0-arduino17/etc/avrdude.conf -v -V -patmega328p -carduino -PCOM8 -b57600 -D -Uflash:w:<filename>.ino.hex:i
```
* Replace `<your login name>` with your windows login.
* Replace `<filename>` with `programNanoNfcConnectedToFox` or `programPuncherId`

# Circuit diagram
![This is a alt text.](/docs/Nano_Schematic_bb.png "Circuit diagram between NFC and Fox")

# Doxygen generated code flow 
## Nano 
![This is a alt text.](docs/program_nano_nfc_connected_to_fox_8ino_cgraph_setup.png "Configure/Setup the Nano, RTC and NFC")
![This is a alt text.](docs/program_nano_nfc_connected_to_fox_8ino_cgraph_loop.png "Nano's loop")
## Android App
![This is a alt text.](docs/classcom_1_1example_1_1grjapp_1_1nfcreadwrite_1_1_main_activity__coll__graph.png "Android App call graph")


# Android Studio version >= 4.1
Use the project import options and follow the instructions as suggested.
- Gradle Build Tool version 4.10. 
- Add all the necessary dependencies that the IDE prompts. The IDE will automatically try to fix these.

# Supported NFC cards:
- Mifare Classic 1k
- Mifare Ultralight

Potentially supported phones where this ARDF GRJ app can run are listed here: https://www.shopnfc.com/en/content/7-nfc-compatibility 

# Screenshots of the App used for Göteborg Radio orienteering
&emsp; <img src="/docs/AppPics/1.png" alt="Allow Unknown sources" width="200px;" />
<img src="/docs/AppPics/2.png" alt="Allow access for NFC and vibration" width="200px;" />
<img src="/docs/AppPics/3.png" alt="Installing..." width="200px;" />
<img src="/docs/AppPics/4.png" alt="App installed" width="200px;" />

&emsp;&ensp;<img src="/docs/AppPics/5.png" alt="ARDF GRJ App icon" width="200px;" />
<img src="/docs/AppPics/6.png" alt="Enable NFC" width="200px;" />
<img src="/docs/AppPics/7.png" alt="App boot up screen" width="200px;" />
<img src="/docs/AppPics/8.png" alt="App reading contents" width="200px;" />

&emsp;&ensp;<img src="/docs/AppPics/9.png" alt="App starting erasing procedure" width="200px;" />

# For Debugging NFC cards: Install "NFC TagInfo by NXP" app on your NFC capable phone
Memory contents of an NFC card can be obtained using the above app. A screenshot of the memory contents and its usage is shown below.

## Memory contents of Mifare Ultralight

&emsp;<img src="/docs/AppPics/Screenshot_TagInfo.png" alt="Memroy contents of Mifare Ultralight NFC card" width="200px;" />
The red box is the content that we are interested in. 

- Consider this entry in Ultralight card memory
`[04] 09 01 00 00`
where
    - `[04]` = Control Memroy Address
    - `09` = Puncher Id
    - `01` = Relative address for next write. Here the next write (Fox punched) will happen at `[04] + 01 = [05]`
    - `00` = Unused
- Consider another entry in Ultralight card memory
`[07] 03 00 0F 08`
where
    - `[07]` = (Control Memroy Address) + Relative Address 
    - `03` = Fox #3
    - `00` = Hours
    - `0F` = Minutes
    - `08` = Seconds

* Cyclic memory spanning from `[05]` to `[0F]` and back to `[05]`.

## Memory contents of Mifare Classic

&emsp;<img src="/docs/AppPics/Screenshot_MifareClassic_TagInfo.jpg" alt="Memroy contents of Mifare Classic NFC card" width="200px;" />

- Consider this entry in Ultralight card memory
`[04] 0C 05`
where
    - `[04]` = Control Memroy Address
    - `0C` = Puncher Id
    - `05` = Absolute address for next write. Here the next write (Fox punched) will happen at `[05]`
- Consider another entry in Ultralight card memory
`[08] 03 00 0F 08`
where
    - `[08]` = (Control Memroy Address) + Relative Address 
    - `03` = Fox #3
    - `00` = Hours
    - `0F` = Minutes
    - `08` = Seconds

* Cyclic memory for fox time spans from `[05]` to `[12]` and back to `[05]`.
    - Sector 1: `[04]`,`[05]`,`[06]`,
    - Sector 2: `[08]`,`[09]`,`[0A]`,
    - Sector 3: `[0C]`,`[0D]`,`[0E]`,
    - Sector 4: `[10]`,`[11]`,`[12]`
    - Skipped memory locations: `[07]`, `[0B]`, `[0F]` as these locations have the key storage

# UART Communication with Fox and Puncher(NFC)
## Fox LED before start of orienteering
<img src="/docs/Logs/1.Fox_LED_Fox_Before_Transmission.PNG" alt="Fox LED before start of orienteering" width="800px;" />

The above figure shows Fox LED blinking at 2 Hz.

## Fox LED Off when Orienteering has started
<img src="/docs/Logs/2.Fox_LED_Off_Fox1_TXing.PNG" alt="Fox LED before start of orienteering" width="800px;" />

The 2 Hz signal goes away with a LOW signal. The MCU sees a high signal due to the inverting transistor to translate a the (0V-2V) LED signal signal to (0V-5V) signal to MCU.

## Puncher 9 successful registration with the Fox with timestamp
<img src="/docs/Logs/3.Puncher9_WithTimeStampFromFox_NFC_0x0909_01_00_0A_2C_37_Good.PNG" alt="Puncher 9 successful registration with the Fox with timestamp" width="800px;" />

At 4800 baud interaction with Fox. Puncher #9 is sent twice `0x09` and `0x09` to the fox. The response from the Fox is `0x01 0x00 0x0A 0x2C 0x37` where `0x01` maps to which Fox it is, here it is Fox 1. While `0x00 0x0A 0x2C` maps to `hh:mm:ss`. Finally, `0x37` is the CRC = `Fox# + hh + mm + ss` = `0x01 + 0x00 + 0x0A + 0x2C = 0x37`

## Repeated punching of puncher 9
<img src="/docs/Logs/4.Puncher9_punchedAgain_NFC_0x0909_01_00_0A_2C_37_Good.PNG" alt="Fox LED before start of orienteering" width="800px;" />

Puncher 9 registering with same fox again will generated a response (`0x7D 0x22 0xE2 0x00 0x00`) .

## Orienteering time finished
<img src="/docs/Logs/5.Fox_LED_Glowing_Fox1_Finished_TXing.PNG" alt="Fox LED before start of orienteering" width="800px;" />

The Fox LED will continously glow, a HIGH signal is observed

# Cyclomatic Complexity

The code was significantly improved with help of [Lizard](https://github.com/terryyin/lizard)
Just rename the .ino file as .cpp and run `lizard`.
