/**@file programNanoNfcConnectedToFox.ino */
/**************************************************************************/
/*!
    @file     readMifare.pde
    @author   Adafruit Industries
	@license  BSD (see license.txt)

    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.

    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:

    - Authenticate block 4 (the first block of Sector 1) using
      the default KEYA of 0XFF 0XFF 0XFF 0XFF 0XFF 0XFF
    - If authentication succeeds, we can then read any of the
      4 blocks in that sector (though only block 4 is read here)

    If the card has a 7-byte UID it is probably a Mifare
    Ultralight card, and the 4 byte pages can be read directly.
    Page 4 is read by default since this is the first 'general-
    purpose' page on the tags.


  This is an example sketch for the Adafruit PN532 NFC/RFID breakout boards
  This library works with the Adafruit NFC breakout
  ----> https://www.adafruit.com/products/364

  Check out the links above for our tutorials and wiring diagrams
  These chips use SPI or I2C to communicate.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

*/
/**************************************************************************/
#include <avr/sleep.h>
#include <avr/power.h>
#include <pins_arduino.h>
#include <Wire.h>

/*
  The fox sends a PWM signal to blink the LED when it is not yet transmitting the morse code
  When the orienteering starts, the PWM signal on the TX line goes low.
  When the orienteering is finished, this signal goes High.
  
  PWM signal period = 2 seconds and with 3 uniform samples, 
  we need to check every 0.666 seconds
  
  Going low signal can be used to turn on the NFC and the RTC.
  Going High signal can be used to turn off the NFC and the RTC.
  See: 
  1) Fox_Before_Transmission.logicdata
  2) Fox1_TXing.logicdata
  3) Fox1_Finished_TXing.logicdata
  
*/

enum nanoPins
{
  // Digital Pins
  PN532_IRQ_PIN = 2,
  FOX_PWM_DETECT_PIN = 3, // D3: Pin to detect the PWM signal, LOW, HIGH from Fox
  LED_PIN = 4,            // D4
  BUZZ_PIN = 5,           // D5
  FOX_RX_PIN = 6,         // D6 (Arduino Tx)
  FOX_TX_PIN = 7,         // D7 (Arduino Rx)
  PN532_RSTPDN_PIN = 8,   // D8: LOW means PN532 is powered off
  PN532_RESET_PIN = 9,    // D9: Not connected in HW by default on the NFC Shield
  // Analog Pins
  // Nano: I2C: A4 (SDA) and A5 (SCL)

  // Below are the unused pins
  UNUSED_PIN_D10 = 10,
  UNUSED_PIN_D11 = 11,
  UNUSED_PIN_D12 = 12,
  UNUSED_PIN_D13 = 13,
  UNUSED_PIN_A0 = PIN_A0,
  UNUSED_PIN_A1 = PIN_A1,
  UNUSED_PIN_A2 = PIN_A2,
  UNUSED_PIN_A3 = PIN_A3,
  UNUSED_PIN_A6 = PIN_A6,
  UNUSED_PIN_A7 = PIN_A7
};

static const uint32_t TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN = 10000; // 10 second non-blocking delay

// TimerInterrrupt library needs the following #defines for using HW timers
#define USE_TIMER_1 false
#define USE_TIMER_2 true // We use timer2 as it can still run when in SLEEP_MODE_PWR_SAVE
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false
#include "TimerInterrupt.h"

/*
  ENABLE_LOGGING  = 1, print all logs
                        Sketch uses 16950 bytes (55%) of program storage space. Maximum is 30720 bytes.
                        Global variables use 1325 bytes (64%) of dynamic memory, leaving 723 bytes for local variables. Maximum is 2048 bytes.

                  = 0, disable all logging
                        Sketch uses 13908 bytes (45%) of program storage space. Maximum is 30720 bytes.
                        Global variables use 697 bytes (34%) of dynamic memory, leaving 1351 bytes for local variables. Maximum is 2048 bytes.

*/
#define ENABLE_LOGGING 1
#if ENABLE_LOGGING
#define Sprint(a) (Serial.print(a))
#define Sprint2(a, x) (Serial.print(a, x))
#define Sprintln(a) (Serial.println(a))
#define Sprintln2(a, x) (Serial.println(a, x))
#else
#define Sprint(a)
#define Sprint2(a, x)
#define Sprintln(a)
#define Sprintln2(a, x)
#endif

/*
  RTC_ENABLE 	= 0 - Disable RTC to save battery
              = 1 - Mostly used as a standalone setup.
*/
#define RTC_ENABLE 0
#if RTC_ENABLE
#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);
#endif

#include <SoftwareSerial.h>
SoftwareSerial foxSerial(FOX_TX_PIN, FOX_RX_PIN); // Arduino RX, Arduino TX. These are D7 and D6 pins
static const uint8_t NUM_BYTES_FROM_FOX = 5;
static const uint8_t SIZE_OF_DATA = 4;

#include <Adafruit_PN532.h>
Adafruit_PN532 nfc(PN532_IRQ_PIN, PN532_RESET_PIN);
static const uint8_t CLASSIC_BLOCK_LEN = 16;

static void printReg()
{
  Sprint(F("PRR:"));
  Sprintln2(PRR, HEX);
  Sprint(F("ADCSRA:"));
  Sprintln2(ADCSRA, HEX);
  Sprint(F("ACSR:"));
  Sprintln2(ACSR, HEX);
  Sprint(F("DIDR1:"));
  Sprintln2(DIDR1, HEX);
  Sprint(F("MCUCR:"));
  Sprintln2(MCUCR, HEX);
  Serial.flush();
}

static void disableUnnecessaryPeripherals()
{
  printReg();

  ADCSRA &= (uint8_t)~_BV(ADEN); // Bit 7 – ADEN: ADC Enable = 1, now disable = 0
  ACSR &= (uint8_t)~_BV(ACIE);   // ACIE needs to be cleared before ACSR
  ACSR |= (uint8_t)_BV(ACD);     // Bit 7 – ACD: Analog Comparator Disable

  power_adc_disable();
  power_spi_disable();

  /* Never enable this code as the delay functions get messed up.
    power_timer0_disable(); // when timer0 is disabled, then the buzzer is stuck
    power_timer1_disable(); // when timer1 is disabled, then timer ISR doesn't fire.
    power_timer2_disable(); // Used but when timer2 is disabled, the Buzzer doesn't work
  */

  printReg();
}

static void rebootNanoNfc()
{
  // TODO: Untested
  // As watchdog reset with old bootloader will result in infinite reboot due to old bootloader bug
  // Other means are merely software reboot whose registers might not be in good state
  sleep_disable(); //disable sleep, awake now

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  detachInterrupt(digitalPinToInterrupt(FOX_PWM_DETECT_PIN));
  detachInterrupt(digitalPinToInterrupt(PN532_IRQ_PIN));
  setup();
}

static void shutdownNanoNfc()
{
  Sprintln(F("Powering down"));

  // This will help the organizer to make sure
  // LED blinks after bootup
  // A constant LED glow is a sign that it has shutdown
  // This is to ensure that NFC doesn't turn off before the orienteering start
  unsigned long timeSinceBootup = millis();
  if (timeSinceBootup < 60000)
  {
    // If we shutting down less than 60 seconds after bootup
    // Probably due to low voltage or coming from a bad state
    // Then inform the user with glowing the LED pin
    // Otherwise, regular orienteering don't waste power 13mA can become 30mA
    Sprint(F("Early Shutdown  = "));
    Sprintln(timeSinceBootup);
    digitalWrite(LED_PIN, HIGH); // LED_PIN
  }
  digitalWrite(PN532_RSTPDN_PIN, LOW); // Power down the PN532

  disableUnnecessaryPeripherals();
  power_all_disable(); // Only looks at PRR register, USART0 will disable printing any more logs

  // In case we fall asleep by mistake, we reboot on interrupt from FOX or in case IRQ from NFC
  pinMode(FOX_PWM_DETECT_PIN, INPUT_PULLUP);
  detachInterrupt(digitalPinToInterrupt(FOX_PWM_DETECT_PIN));
  EIFR = (1 << INTF1);
  attachInterrupt(digitalPinToInterrupt(FOX_PWM_DETECT_PIN), rebootNanoNfc, HIGH);

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_mode();
  // Nothing after this as CPU sleeps; Not configured to wake up after this.
}

static void checkVccVoltageToProceedOrShutdown()
{
  // TODO: Untested
  // Snipped from https://forum.arduino.cc/index.php?topic=331178.msg2285210#msg2285210
  // See Figure 23-1. Analog to Digital Converter Block Schematic Operation in 328 boards
  static const long INTERNALREFERENCEVOLTAGE = 1056L; // Adjust this value to your boards specific internal BG voltage x1000
                                                      // REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc external reference
                                                      // MUX3 MUX2 MUX1 MUX0  --> 1110 1.1V (VBG)         -Selects channel 14, bandgap voltage, to measure
  ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (0 << MUX0);
  delay(50); // Let mux settle a little to get a more stable A/D conversion
             // Start a conversion
  ADCSRA |= _BV(ADSC);
  // Wait for it to complete
  while (((ADCSRA & (1 << ADSC)) != 0))
    ;
  // Scale the value
  uint32_t battVolts = (((INTERNALREFERENCEVOLTAGE * 1024L) / ADC) + 5L) / 10L; // calculates for straight line value
  Sprint(F("Battery Vcc (x100) =  "));
  Sprintln(battVolts);
  if (battVolts < 450)
  {
    // 16 MHz operating frequency requires 4.5V, we cannot operate lower than this
    shutdownNanoNfc();
  }
  return;
}

static void enableDetectionOrienteeringEnd()
{
  // Must clear any pending interrupts before using INT1
  // Otherwise, orienteer punching will glow the LED
  EIFR = (1 << INTF1);
  // Detach the timer
  ITimer2.detachInterrupt();
  Sprintln(F("Timer expired: Enabled orienteering end detection"));
  // Configure interrupt for the FOX_PWM_DETECT_PIN to go LOW (after Robert's inverting transistor) after the orienteering.
  attachInterrupt(digitalPinToInterrupt(FOX_PWM_DETECT_PIN), shutdownNanoNfc, LOW);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
static void detectPWMLowSignalFromFox()
{
  while (1)
  {
    unsigned long duration = pulseInLong(FOX_PWM_DETECT_PIN, HIGH, 4e6); // 4 seconds
    Sprint(F("PWM High duration (~1 second) = "));
    Sprintln(duration);
    Serial.flush();
    if (duration == 0)
    {
      // HIGH signal not detected for 4 seconds
      // the fox has started morse code transmission, so configure NFC and RTC via normal bootup
      ITimer2.init();

      if (ITimer2.attachInterruptInterval(TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN, enableDetectionOrienteeringEnd))
      {
        Sprint(F("Start  ITimer2 OK, millis() = "));
        Sprintln(millis());
      }
      else
      {
        Sprintln(F("Can't set ITimer2. Select another freq., duration or timer"));
      }
      break;
    }
  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#if RTC_ENABLE
#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime &dt)
{
  char datestring[20];

  snprintf_P(datestring,
             countof(datestring),
             PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
             dt.Month(),
             dt.Day(),
             dt.Year(),
             dt.Hour(),
             dt.Minute(),
             dt.Second());
  Sprint(datestring);
}

void setupRTC(void)
{
  //--------RTC SETUP ------------
  Rtc.Begin();

  // if you are using ESP-01 then uncomment the line below to reset the pins to
  // the available pins for SDA, SCL
  // Wire.begin(0, 2); // due to limited pins, use pin 0 and 2 for SDA, SCL

  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
  Sprintln();

  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) first time you ran and the device wasn't running yet
    //    2) the battery on the device is low or even missing

    Sprintln(F("RTC lost confidence in the DateTime!"));

    // following line sets the RTC to the date & time this sketch was compiled
    // it will also reset the valid flag internally unless the Rtc device is
    // having an issue

    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
    Sprintln(F("RTC was not actively running, starting now"));
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled)
  {
    Sprintln(F("RTC is older than compile time!  (Updating DateTime)"));
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled)
  {
    Sprintln(F("RTC is newer than compile time. (this is expected)"));
  }
  else if (now == compiled)
  {
    Sprintln(F("RTC is the same as compile time! (not expected but all is fine)"));
  }

  // never assume the Rtc was last configured by you, so
  // just clear them to your needed state
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void RTCReadOut()
{
  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) the battery on the device is low or even missing and the power line was disconnected
    Sprintln(F("RTC lost confidence in the DateTime!"));
  }

  RtcDateTime now = Rtc.GetDateTime();
  printDateTime(now);
  Sprintln();

  RtcTemperature temp = Rtc.GetTemperature();
  Sprint(temp.AsFloatDegC());
  Sprintln(F("C"));
}
#endif
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
static void ShowLEDNotification(unsigned long msDuration)
{
  // This is to show repeated punching, so no buzzer, just the LED like in the fox
  digitalWrite(LED_PIN, HIGH); // turn the LED on (HIGH is the voltage level)
  delay(msDuration);           // Wait a bit before reading the card again
  digitalWrite(LED_PIN, LOW);
}

static void ShowNotification(unsigned long msDuration)
{
  analogWrite(BUZZ_PIN, 127); // 50% duty cycle. Removed use of Tone() as it interferes with ITimer2
  ShowLEDNotification(msDuration);
  digitalWrite(BUZZ_PIN, LOW);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
static void printMifareClassicCardBlock(uint8_t currentblockIndex, uint8_t absIndex,
                                        uint8_t data[], uint32_t blockNumber,
                                        bool success)
{
  if (success)
  {
    if (currentblockIndex != 0xFF)
    {
      Sprint(F("Block "));
      Sprint2(blockNumber, HEX);
      if (blockNumber < 0x10)
      {
        Sprint(F("  "));
      }
      else
      {
        Sprint(F(" "));
      }
      // Dump the raw data
      nfc.PrintHexChar(data, CLASSIC_BLOCK_LEN);
    }
    else
    {
      Sprint(F("Block "));
      Sprint2(absIndex, HEX);
      if (absIndex < 10)
      {
        Sprint(F("  "));
      }
      else
      {
        Sprint(F(" "));
      }
      // Dump the raw data
      nfc.PrintHexChar(data, CLASSIC_BLOCK_LEN);
    }
  }
  else
  {
    // Oops ... something happened
    Sprint(F("Block "));
    Sprint2(blockNumber, DEC);
    Sprintln(F(" unable to read this block or skip"));
  }
}

static bool tellFoxPuncherIdGetFoxTime(uint8_t puncherId, uint8_t rxByte[])
{
  bool flagSuccessfullyUpdatedFox = false;

  foxSerial.write(puncherId); // Return variable not checked but we check with fox's response
  foxSerial.write(puncherId); // Return variable not checked but we check with fox's response

  for (uint8_t iByteFox = 0; iByteFox < NUM_BYTES_FROM_FOX; iByteFox++)
  {
    if (foxSerial.available())
    {
      // get the byte from the software serial port
      int byteFox = foxSerial.read();
      if (byteFox != -1)
      {
        rxByte[iByteFox] = (uint8_t)byteFox;
      }
      else
      {
        Sprintln(F("Fox serial read failed!"));
      }
    }
  }

  /*
  //Fox response for unit testing
  rxByte[0] = 1;
  rxByte[1] = 2;
  rxByte[2] = 3;
  rxByte[3] = 4;
  rxByte[4] = 0xA;
  //*/
  Sprintln2(rxByte[0], HEX);
  Sprintln2(rxByte[1], HEX);
  Sprintln2(rxByte[2], HEX);
  Sprintln2(rxByte[3], HEX);
  Sprintln2(rxByte[4], HEX);

  static const uint8_t uglySignalFromFox[3] = {0x7D, 0x22, 0xE2};

  if ((rxByte[0] | rxByte[1] | rxByte[2] | rxByte[3]) == 0)
  {
    // When there is NO response from FOX or when there is NO FOX connected.
  }
  else if (0 == memcmp(rxByte, uglySignalFromFox, sizeof(uglySignalFromFox)))
  {
    // No idea why the Fox sends out this ugly signal, we acknowledge and skip writing to NFC
    foxSerial.write(0x6F); // TODO: Return variable not checked
    ShowLEDNotification(100);
  }
  else if ((rxByte[0] + rxByte[1] + rxByte[2] + rxByte[3]) == rxByte[4])
  {
    // Successful in talking to fox so tell the fox that the puncher was registered
    flagSuccessfullyUpdatedFox = true;
    foxSerial.write(0x6F); // TODO: Return variable not checked
  }
  else
  {
    Sprintln(F("Unknown response from Fox"));
  }

  return (flagSuccessfullyUpdatedFox);
}

static bool authClassicBlock(uint8_t *uid, uint8_t uidLength, uint32_t blockNumber)
{
  // Keyb on NDEF and Mifare Classic should be the same
  static uint8_t keyuniversal[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  bool success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, blockNumber, 0, keyuniversal);
  if (!success)
  {
    // Try keyb
    Sprintln(F("keya failed, trying keyb on Block: "));
    Sprint2(blockNumber, DEC);
    success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, blockNumber, 1, keyuniversal);
    if (!success)
    {
      // Both keys failed
      Sprintln(F("Both keys failed!"));
    }
  }
  return success;
}

static void updateClassicCardWithNewAbsIndex(uint8_t newAbsIndex, uint8_t data[],
                                             uint8_t puncherId)
{
  // Due to validBlocksForFoxes, block = 4 maps to index = 0
  uint8_t blockNumber = 4;
  uint8_t currentblockIndex = 0;
  // Step 4: Update Block 4 with the newAbsIndex
  // Update/wrap around absIndex and break; after printing its contents
  Sprintln(F("Step 4"));
  uint8_t writeDataToCard[CLASSIC_BLOCK_LEN] = {0};
  memset(writeDataToCard, 0, sizeof(writeDataToCard));
  memcpy(writeDataToCard, data, CLASSIC_BLOCK_LEN * sizeof(uint8_t));

  //Update absIndex to the newAbsIndex in Block 4
  if (newAbsIndex == 4)
  {
    newAbsIndex = 5;
  }
  writeDataToCard[0] = puncherId;
  writeDataToCard[1] = newAbsIndex;

  nfc.mifareclassic_WriteDataBlock(blockNumber, writeDataToCard);
  // The above return variable not checked but we check it with the below read instead
  bool success = nfc.mifareclassic_ReadDataBlock(blockNumber, data);
  printMifareClassicCardBlock(currentblockIndex, newAbsIndex, data, blockNumber, success);

  if (success)
  {
    //flagPrintAndBreak = false;        // CWE-563: This will automatically be cleaned up for the next loop()
    if ((puncherId == data[0]) && (newAbsIndex == data[1]))
    {
      // Successfully saved time to Fox
      ShowNotification(500);
    }
  }
}

static void classicCardWriteFoxTimeToCard(uint8_t *uid, uint8_t uidLength)
{
  // Do not edit validBlocksForFoxes
  static const uint8_t NUMPUNCHED = 11;                      //11 is the maximum user write data to align with ultralight
  static const uint8_t validBlocksForFoxes[NUMPUNCHED + 1] = // In Hex
      {
          0x04, 0x05, 0x06, // Sector 1
          0x08, 0x09, 0x0A, // Sector 2
          0X0C, 0x0D, 0x0E, // Sector 3
          0x10, 0x11, 0x12  // Sector 4
      };

  uint8_t data[CLASSIC_BLOCK_LEN] = {0}; // Array to store block data during reads
  uint8_t puncherId = 0;
  uint8_t absIndex = 5; // First writable location: Block 5
  uint8_t rxByte[NUM_BYTES_FROM_FOX] = {0};
  bool flagSuccessfullyUpdatedFox = false;
  uint8_t writeDataToCard[CLASSIC_BLOCK_LEN] = {0};

  // We probably have a Mifare Classic card ...
  Sprintln(F("Mifare Classic"));

  // The following part is adapted from mifareclassic_memdump.ino
  // Now we try to go through all 16 sectors (each having 4 blocks)
  // authenticating each sector, and then dumping the blocks
  for (uint8_t currentblockIndex = 0; currentblockIndex < sizeof(validBlocksForFoxes); currentblockIndex++)
  {
    bool success = authClassicBlock(uid, uidLength, validBlocksForFoxes[currentblockIndex]);

    if (success)
    {
      if (validBlocksForFoxes[currentblockIndex] == 4)
      {
        // Step 1: Read puncherId and absIndex from card, that is Block 4
        Sprintln(F("Step 1"));
        // Authenticated ... we should be able to read the block now
        // Dump the data into the 'data' array
        success = nfc.mifareclassic_ReadDataBlock(validBlocksForFoxes[currentblockIndex], data);
        puncherId = data[0];
        absIndex = data[1];

        // Step 2: Talk to Fox
        Sprintln(F("Step 2"));
        flagSuccessfullyUpdatedFox = tellFoxPuncherIdGetFoxTime(puncherId, rxByte);
        if (flagSuccessfullyUpdatedFox == false)
        {
          break;
        }
      }
      else if ((validBlocksForFoxes[currentblockIndex] == absIndex) && (flagSuccessfullyUpdatedFox == true))
      {
        flagSuccessfullyUpdatedFox = false;
        // Step 3: Write fox-time to Card
        memset(writeDataToCard, 0, sizeof(writeDataToCard));
        memcpy(writeDataToCard, rxByte, SIZE_OF_DATA * sizeof(uint8_t));
        nfc.mifareclassic_WriteDataBlock(absIndex, writeDataToCard);
        // The above return variable not checked but we check it with the below read instead
        success = nfc.mifareclassic_ReadDataBlock(absIndex, data);
        printMifareClassicCardBlock(currentblockIndex, absIndex, data,
                                    validBlocksForFoxes[currentblockIndex], success);

        uint8_t newAbsIndex = validBlocksForFoxes[(currentblockIndex + 1) % (NUMPUNCHED + 1)];
        Sprint(F("Step 3: newAbsIndex: "));
        Sprintln2(newAbsIndex, HEX);
        // Going to Step 4 below to update block4 which is validBlocksForFoxes[0]
        success = authClassicBlock(uid, uidLength, validBlocksForFoxes[0]);
        if (success)
        {
          updateClassicCardWithNewAbsIndex(newAbsIndex, data, puncherId);
          // Finished updating newAbsIndex to the card so exit
        }
        else
        {
          // Probably the user moved way?
          Sprint(F("Failed to update Classic card"));
        }

        break;
      }
      else
      {
        // Skip this unnecessary read for fast user response for orienteers
        //success = nfc.mifareclassic_ReadDataBlock(validBlocksForFoxes[currentblockIndex], data);
      }
      // Read successful
      printMifareClassicCardBlock(currentblockIndex, absIndex, data, validBlocksForFoxes[currentblockIndex], success);
    }
    else
    {
      Sprintln(F("Auth failed: Try another key?"));
    }
  }
}

static void ultralightCardWriteFoxTimeToCard()
{
  // Note that Authentication is not needed here
  // Try to read the first general-purpose user page (#4)
  Sprintln(F("Reading page 4"));
  static const uint8_t USERSTART = 4;       // For ultralight card, user data start
  static const uint8_t SAFEUSERSTOP = 0x0F; // For ultralight card, user data stop = 39, ultralight card = 0x0F

  uint8_t data[SIZE_OF_DATA] = {0};
  bool success = nfc.mifareultralight_ReadPage(USERSTART, data);
  if (success)
  {
    // Data seems to have been read ... spit it out
    nfc.PrintHexChar(data, SIZE_OF_DATA);
    uint8_t puncherId = data[0];
    uint8_t indexToWriteFoxData = data[1];

    uint8_t rxByte[NUM_BYTES_FROM_FOX] = {0};
    bool flagSuccessfullyUpdatedFox = tellFoxPuncherIdGetFoxTime(puncherId, rxByte);
    if (flagSuccessfullyUpdatedFox == true)
    {
      nfc.mifareultralight_WritePage((uint8_t)(USERSTART + data[1]), rxByte); // we only write rxByte[0] to rxByte[3]

      /* Book keeping the index for the next fox punch.
          This gymnastics is needed as the hunter might repunch
          the same fox again then we might overwrite the previous attempt.
          So, increment and also record the repunch. 
          But, we don't know which fox. Could be one of the previously punched fox.
          */
      data[1] = (uint8_t)(data[1] + 1);
      if (data[1] > (SAFEUSERSTOP - USERSTART))
      {
        data[1] = 1;
      }
      nfc.mifareultralight_WritePage(USERSTART + 0, data);
      // The above return variable not checked but we check it with the below read instead
      memset(data, 0, sizeof(data));
      success = nfc.mifareultralight_ReadPage(USERSTART + 0, data);
      if (success)
      {
        // Data seems to have been read ... spit it out
        nfc.PrintHexChar(data, SIZE_OF_DATA);
        if ((puncherId == data[0]) && ((indexToWriteFoxData + 1) == data[1]))
        {
          // Successfully cleared factor set the puncher
          ShowNotification(500);
        }
      }
#if RTC_ENABLE
      RTCReadOut();
#endif
    }
  }
  else
  {
    Sprintln(F("Read failed"));
  }
}

// cppcheck-suppress unusedFunction
void setup(void)
{
  // NEVER ENABLE WATCHDOG, as the old bootloader has a bug and it wil be infinite reboot

  pinMode(PN532_RSTPDN_PIN, OUTPUT);
  digitalWrite(PN532_RSTPDN_PIN, LOW); // Biggest power hogger so shut it down
  pinMode(FOX_PWM_DETECT_PIN, INPUT_PULLUP);

  pinMode(LED_PIN, OUTPUT);
  pinMode(BUZZ_PIN, OUTPUT);

  // The following are unused pins, so pull up to save power: D10-D13 and A0-A3 and A6-A7
  pinMode(UNUSED_PIN_D10, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D11, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D12, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D13, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A0, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A1, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A2, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A3, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A6, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A7, INPUT_PULLUP);

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  Serial.begin(115200);
  Sprint(F("compiled: "));
  Sprint(__DATE__);
  Sprintln(__TIME__);

  checkVccVoltageToProceedOrShutdown(); // Might shutdown if low Voltage on bootup

  disableUnnecessaryPeripherals();

  ShowNotification(500); // In reality to ensure its working
  detectPWMLowSignalFromFox();

#if RTC_ENABLE
  // Initialize the RTC
  setupRTC();
#endif

  // Initialize the serial port to the Fox
  foxSerial.begin(4800);

  // Initialize the NFC
  pinMode(PN532_RESET_PIN, OUTPUT);
  digitalWrite(PN532_RSTPDN_PIN, HIGH);
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata)
  {
    Sprintln(F("Didn't find PN532"));
    shutdownNanoNfc();
  }
  // Got ok data, print it out!
  Sprint(F("Found chip PN5"));
  Sprintln2((versiondata >> 24) & 0xFF, HEX);
  Sprint(F("Firmware ver. "));
  Sprint2((versiondata >> 16) & 0xFF, DEC);
  Sprint('.');
  Sprintln2((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig(); // TODO: return variable not checked

  Sprintln(F("Waiting for card.."));
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#if 0 // First ensure that we work as normal then enable and test
// TODO: Untested
static void savePowerUntilCardComesInField()
{
  sleep_disable(); //disable sleep, awake now

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
  // Disable this ISR so that the PN532 library can proceed with its normal activity
  // This ISR merely wakes up the CPU when the card comes into the field
  // cppcheck-suppress knownArgument
  detachInterrupt(digitalPinToInterrupt(PN532_IRQ_PIN)); // CWE-570: Ignore this warning
}
#endif

// cppcheck-suppress unusedFunction
void loop(void)
{
  bool success = false;
  uint8_t uid[32];   // Buffer to store the returned UID
  uint8_t uidLength; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

#if 0 // First ensure that we work as normal then enable and test
  // Option1: Unknown behavior for repeated PN532_COMMAND_INLISTPASSIVETARGET
  // TODO: Untested
  // pinMode(PN532_IRQ_PIN, INPUT_PULLUP); // The library doesn't PULLUP otherwise floating IRQs
  byte pn532_packetbuffer[8] = {0}; // PN532_PACKBUFFSIZ
  pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
  pn532_packetbuffer[1] = 1; // Look for 1-card
  pn532_packetbuffer[2] = 0; // Baudrate for our cards
  bool flagListPassiveCards = nfc.sendCommandCheckAck(pn532_packetbuffer, 3);
  Sprint(F("Passive mode = "));
  Sprintln(flagListPassiveCards);
  /*
  // Option2: This might not work
  // Analog front end is shutdown! Unsure how passive card can create field
  // TODO: Untested
  pn532_packetbuffer[0] = PN532_COMMAND_POWERDOWN;
  pn532_packetbuffer[1] = 0x88; // b7 = I2C; b3 = RF level detector sends IRQ
  pn532_packetbuffer[2] = 1;    // Generate IRQ when ext. RF field is detected
  bool flagListPassiveCards = nfc.sendCommandCheckAck(pn532_packetbuffer, 3)
  if (flagListPassiveCards)
  {
    // read data packet
    nfc.readdata(pn532_packetbuffer, 8);
    Sprint("NFC softPowerDown = ");
    Sprintln((pn532_packetbuffer[6] == 0x17));
  }
  */
  if (flagListPassiveCards)
  {
    // Atmel328P can go to power save mode as we have nothing else to do
    // but wait for card to come in the field
    attachInterrupt(digitalPinToInterrupt(PN532_IRQ_PIN), savePowerUntilCardComesInField, LOW);
    set_sleep_mode(SLEEP_MODE_PWR_SAVE);
    sleep_mode();
    /*---ZZZZZZZZ until we get IRQ from NFC, but ITimer2 will run until expiry then ZZZZ */
  }

#endif

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success)
  {
    // cppcheck-suppress knownArgument
    detachInterrupt(digitalPinToInterrupt(FOX_PWM_DETECT_PIN)); // CWE-570: Ignore this warning
    ITimer2.detachInterrupt();

    // Display some basic information about the card
    Sprintln(F("Found card"));
    Sprint(F("  UID Length: "));
    Sprint2(uidLength, DEC);
    Sprintln(F(" bytes"));
    Sprint(F("  UID Value: "));
    nfc.PrintHex(uid, uidLength);
    Sprintln(F(""));

    switch (uidLength)
    {
    case 4:
      Sprintln(F("Mifare Classic"));
      classicCardWriteFoxTimeToCard(uid, uidLength);
      break;
    case 7:
      Sprintln(F("Mifare Ultralight"));
      ultralightCardWriteFoxTimeToCard();
      break;
    default:
      Sprintln(F("Unknown card"));
      break;
    }
    // Re-enable the timer to ensure power down when orienteering finishes
    ITimer2.reattachInterrupt(TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN);
  }
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
