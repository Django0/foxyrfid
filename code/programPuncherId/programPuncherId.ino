/**************************************************************************/
/*!
    @file     readMifare.pde
    @author   Adafruit Industries
	@license  BSD (see license.txt)

    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.

    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:

    - Authenticate block 4 (the first block of Sector 1) using
      the default KEYA of 0XFF 0XFF 0XFF 0XFF 0XFF 0XFF
    - If authentication succeeds, we can then read any of the
      4 blocks in that sector (though only block 4 is read here)

    If the card has a 7-byte UID it is probably a Mifare
    Ultralight card, and the 4 byte pages can be read directly.
    Page 4 is read by default since this is the first 'general-
    purpose' page on the tags.


  This is an example sketch for the Adafruit PN532 NFC/RFID breakout boards
  This library works with the Adafruit NFC breakout
  ----> https://www.adafruit.com/products/364

  Check out the links above for our tutorials and wiring diagrams
  These chips use SPI or I2C to communicate.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

*/
/**************************************************************************/
//#define PUNCHER_ID  (0x01)          // Program the puncherId in Hex (0x00 - 0x7F)
uint8_t indexToWriteFoxDataInUltralight = 0x01;    // Used for relative indexing Ultralight. DO NOT CHANGE
uint8_t indexToWriteFoxDataInMifareClassic = 0x05; // Used for absolute indexing MifareClassic. DO NOT CHANGE
/**************************************************************************/
#include <Wire.h>
#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);

#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ (2)
#define PN532_RESET (3) // Not connected by default on the NFC Shield

Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

#define BUZZ_PIN (3)
#define LED_PIN (4)

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setupRTC(void)
{
  Serial.print("compiled: ");
  Serial.print(__DATE__);
  Serial.println(__TIME__);
  //--------RTC SETUP ------------
  Rtc.Begin();

  // if you are using ESP-01 then uncomment the line below to reset the pins to
  // the available pins for SDA, SCL
  // Wire.begin(0, 2); // due to limited pins, use pin 0 and 2 for SDA, SCL

  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
  Serial.println();

  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) first time you ran and the device wasn't running yet
    //    2) the battery on the device is low or even missing

    Serial.println("RTC lost confidence in the DateTime!");

    // following line sets the RTC to the date & time this sketch was compiled
    // it will also reset the valid flag internally unless the Rtc device is
    // having an issue

    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
    Serial.println("RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled)
  {
    Serial.println("RTC is older than compile time!  (Updating DateTime)");
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled)
  {
    Serial.println("RTC is newer than compile time. (this is expected)");
  }
  else if (now == compiled)
  {
    Serial.println("RTC is the same as compile time! (not expected but all is fine)");
  }

  // never assume the Rtc was last configured by you, so
  // just clear them to your needed state
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void RTCReadOut()
{
  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) the battery on the device is low or even missing and the power line was disconnected
    Serial.println("RTC lost confidence in the DateTime!");
  }

  RtcDateTime now = Rtc.GetDateTime();
  printDateTime(now);
  Serial.println();

  RtcTemperature temp = Rtc.GetTemperature();
  Serial.print(temp.AsFloatDegC());
  Serial.println("C");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setup(void)
{
  Serial.begin(115200);
  delay(1000);
  // Initialize the RTC
  setupRTC();

  // Initialize the NFC
  Serial.println("Hello!");
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata)
  {
    Serial.print("Didn't find PN53x board");
    while (1)
      ; // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5");
  Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. ");
  Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.');
  Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("Waiting for an ISO14443A Card ...");
  pinMode(LED_PIN, OUTPUT);
  ShowNotification();
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void loop(void)
{
  const byte numChars = 32;
  char receivedChars[numChars]; // an array to store the received user input that is puncherId data

  byte incomingBytePuncherId = 0; // Converted input string to puncher Id
  Serial.println("\n\nEnter puncher ID (enter value between: 1..127)");
  boolean newData = false;
  while (newData == false)
  {
    recvWithEndMarker(&newData, receivedChars, numChars);
    incomingBytePuncherId = showNewNumber(&newData, receivedChars);
  }
  // Wait a bit before trying again
  if (newData == true)
  {
    newData = false;
    Serial.println("Waiting for puncher...");
    uint8_t keya[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    uint8_t success;
    uint8_t uid[16];   // Buffer to store the returned UID
    uint8_t uidLength; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

    // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
    // 'uid' will be populated with the UID, and uidLength will indicate
    // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
    success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

    if (success)
    {
      // Display some basic information about the card
      Serial.println("Found an ISO14443A card");
      Serial.print("  UID Length: ");
      Serial.print(uidLength, DEC);
      Serial.println(" bytes");
      Serial.print("  UID Value: ");
      nfc.PrintHex(uid, uidLength);
      Serial.println("");

      if (uidLength == 4)
      {
        uint8_t currentblockIndex; // Counter to keep track of which block we're on
        uint8_t data[16];          // Array to store block data during reads
        // Do not edit validBlocksForFoxes
        const uint8_t validBlocksForFoxes[] =
            {
                4, 5, 6,    // Sector 1
                8, 9, 10,   // Sector 2
                12, 13, 14, // Sector 3
                16, 17, 18  // Sector 4
            };
        // We probably have a Mifare Classic card ...
        Serial.println("Seems to be a Mifare Classic card (4 byte UID)");

        // The following part is adapted from mifareclassic_memdump.ino
        // Now we try to go through all 16 sectors (each having 4 blocks)
        // authenticating each sector, and then dumping the blocks
        for (currentblockIndex = 0; currentblockIndex < sizeof(validBlocksForFoxes); currentblockIndex++)
        {
          success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, validBlocksForFoxes[currentblockIndex], 0, keya);
          if (!success)
          {
            // Try keyb
            Serial.println("keya failed, trying keyb on Block: ");
            Serial.print(validBlocksForFoxes[currentblockIndex], DEC);

            success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, validBlocksForFoxes[currentblockIndex], 1, keya);
          }

          if (success)
          {
// Write our test data here
#define BLOCK_LEN (16)
            uint8_t writeDataToCard[BLOCK_LEN];
            memset(writeDataToCard, 0x0, BLOCK_LEN * sizeof(uint8_t));
            if (validBlocksForFoxes[currentblockIndex] == 4)
            {
              writeDataToCard[0] = incomingBytePuncherId;
              writeDataToCard[1] = indexToWriteFoxDataInMifareClassic;
            }

            nfc.mifareclassic_WriteDataBlock(validBlocksForFoxes[currentblockIndex], writeDataToCard);

            // Authenticated ... we should be able to read the block now
            // Dump the data into the 'data' array
            success = nfc.mifareclassic_ReadDataBlock(validBlocksForFoxes[currentblockIndex], data);
            if (success)
            {
              // Read successful
              Serial.print("Block ");
              Serial.print(validBlocksForFoxes[currentblockIndex], DEC);
              if (validBlocksForFoxes[currentblockIndex] < 10)
              {
                Serial.print("  ");
              }
              else
              {
                Serial.print(" ");
              }
              // Dump the raw data
              nfc.PrintHexChar(data, 16);
            }
            else
            {
              // Oops ... something happened
              Serial.print("Block ");
              Serial.print(validBlocksForFoxes[currentblockIndex], DEC);
              Serial.println(" unable to read this block");
            }
          }
          else
          {
            Serial.println("Ooops ... authentication failed: Try another key?");
          }
        }
      }

      if (uidLength == 7)
      {
        // We probably have a Mifare Ultralight card ...
        Serial.println("Seems to be a Mifare Ultralight tag (7 byte UID)");
        Serial.println("Trying to authenticate block 4 with default KEYA value");
        success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4 + 0, 0, keya);

        // Try to read the first general-purpose user page (#4)
        Serial.println("Reading page 4");

#define SIZE_OF_DATA (4)
#define USER_START (4)        // For NTAG213 ultralight card, user data start
#define SAFE_USER_STOP (0x0F) // For NTAG213 user data stop = 39, ultralight card = 0x0F

        uint8_t data[SIZE_OF_DATA];
        for (uint8_t iPage = 0; iPage < SAFE_USER_STOP; iPage++)
        {
          success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, USER_START + iPage, 0, keya);
          if (success)
          {
            success = nfc.mifareultralight_ReadPage(USER_START + iPage, data);
            if (success)
            {
              // Data seems to have been read ... spit it out
              nfc.PrintHexChar(data, SIZE_OF_DATA);
              Serial.println("");
              uint8_t writePuncherID[SIZE_OF_DATA];
              memset(writePuncherID, 0x0, SIZE_OF_DATA * sizeof(uint8_t));

              if (iPage == 0)
              {
                writePuncherID[0] = incomingBytePuncherId;
                writePuncherID[1] = indexToWriteFoxDataInUltralight;
              }
              success = nfc.mifareultralight_WritePage(USER_START + iPage, writePuncherID);
            }
          }
          else
          {
            Serial.println("Ooops ... unable to read the requested page!?");
            break;
          }
        }
        success = nfc.mifareultralight_ReadPage(USER_START + 0, data);
        if (success)
        {
          // Data seems to have been read ... spit it out
          nfc.PrintHexChar(data, SIZE_OF_DATA);
          if ((incomingBytePuncherId == data[0]) && (indexToWriteFoxDataInUltralight == data[1]))
          {
            // Successfully cleared factor set the puncher
            ShowNotification();
          }
        }
        RTCReadOut();
        // Wait a bit before reading the card again
        delay(1000);
      }
    }
  }
}

void recvWithEndMarker(boolean *newData, char receivedChars[], byte numChars)
{
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;

  if (Serial.available() > 0)
  {
    rc = Serial.read();

    if (rc != endMarker)
    {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars)
      {
        ndx = numChars - 1;
      }
    }
    else
    {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      *newData = true;
    }
  }
}

int showNewNumber(boolean *newData, char receivedChars[])
{
  int retDataNumber = 0;
  if (*newData == true)
  {

    retDataNumber = atoi(receivedChars);
    Serial.print("This just in ... ");
    Serial.println(receivedChars);
    Serial.print("Data as Number ... ");
    Serial.println(retDataNumber);
  }
  return retDataNumber;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void ShowNotification(void)
{
  tone(BUZZ_PIN, 440);
  digitalWrite(LED_PIN, HIGH); // turn the LED on (HIGH is the voltage level)
  delay(500);                  // Wait a bit before reading the card again
  noTone(BUZZ_PIN);
  digitalWrite(LED_PIN, LOW);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime &dt)
{
  char datestring[20];

  snprintf_P(datestring,
             countof(datestring),
             PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
             dt.Month(),
             dt.Day(),
             dt.Year(),
             dt.Hour(),
             dt.Minute(),
             dt.Second());
  Serial.print(datestring);
}
