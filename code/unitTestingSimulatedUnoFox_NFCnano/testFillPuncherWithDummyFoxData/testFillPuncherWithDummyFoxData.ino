/**************************************************************************/
/*!
    @file     readMifare.pde
    @author   Adafruit Industries
  @license  BSD (see license.txt)

    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.

    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:

    - Authenticate block 4 (the first block of Sector 1) using
      the default KEYA of 0XFF 0XFF 0XFF 0XFF 0XFF 0XFF
    - If authentication succeeds, we can then read any of the
      4 blocks in that sector (though only block 4 is read here)

    If the card has a 7-byte UID it is probably a Mifare
    Ultralight card, and the 4 byte pages can be read directly.
    Page 4 is read by default since this is the first 'general-
    purpose' page on the tags.


  This is an example sketch for the Adafruit PN532 NFC/RFID breakout boards
  This library works with the Adafruit NFC breakout
  ----> https://www.adafruit.com/products/364

  Check out the links above for our tutorials and wiring diagrams
  These chips use SPI or I2C to communicate.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

*/
/**************************************************************************/
#define PUNCHER_ID (0x0C)
uint8_t indexToWriteFoxData = 0x01;
// Test Data to write to puncher
#define SIZE_OF_DATA (4)
#define NUM_PUNCHED (11) //11 is the maximum user write data
#define DATA_FOX_HHMMSS (4)

uint8_t testDataFromFox[NUM_PUNCHED][DATA_FOX_HHMMSS] = {{0x01, 0x00, 0x0A, 0x2C}, // FoxID, HH, MM, SS
                                                         {0x02, 0x00, 0x0D, 0x26},
                                                         {0x03, 0x00, 0x0F, 0x08},
                                                         {0x04, 0x00, 0x10, 0x39},
                                                         //{0x7D, 0x22, 0xE2, 0x00}, // Repeated Fox punched based on 0x7D, avoid writing this as it seems to corrupt the card
                                                         {0x05, 0x01, 0x0A, 0x2C},
                                                         {0x06, 0x01, 0x0A, 0x2C},
                                                         {0x07, 0x01, 0x0A, 0x2C},
                                                         {0x08, 0x01, 0x0A, 0x2C},
                                                         {0x09, 0x01, 0x0A, 0x2C},
                                                         {0x0A, 0x01, 0x0A, 0x2C},
                                                         {0x0B, 0x01, 0x0A, 0x2C}};

/**************************************************************************/
#include <Wire.h>
#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);

#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ (2)
#define PN532_RESET (3) // Not connected by default on the NFC Shield

Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

#define BUZZ_PIN (3)
#define LED_PIN (4)

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setupRTC(void)
{
  Serial.print("compiled: ");
  Serial.print(__DATE__);
  Serial.println(__TIME__);
  //--------RTC SETUP ------------
  Rtc.Begin();

  // if you are using ESP-01 then uncomment the line below to reset the pins to
  // the available pins for SDA, SCL
  // Wire.begin(0, 2); // due to limited pins, use pin 0 and 2 for SDA, SCL

  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
  Serial.println();

  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) first time you ran and the device wasn't running yet
    //    2) the battery on the device is low or even missing

    Serial.println("RTC lost confidence in the DateTime!");

    // following line sets the RTC to the date & time this sketch was compiled
    // it will also reset the valid flag internally unless the Rtc device is
    // having an issue

    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
    Serial.println("RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled)
  {
    Serial.println("RTC is older than compile time!  (Updating DateTime)");
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled)
  {
    Serial.println("RTC is newer than compile time. (this is expected)");
  }
  else if (now == compiled)
  {
    Serial.println("RTC is the same as compile time! (not expected but all is fine)");
  }

  // never assume the Rtc was last configured by you, so
  // just clear them to your needed state
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void RTCReadOut()
{

  if (!Rtc.IsDateTimeValid())
  {
    // Common Cuases:
    //    1) the battery on the device is low or even missing and the power line was disconnected
    Serial.println("RTC lost confidence in the DateTime!");
  }

  RtcDateTime now = Rtc.GetDateTime();
  printDateTime(now);
  Serial.println();

  RtcTemperature temp = Rtc.GetTemperature();
  Serial.print(temp.AsFloatDegC());
  Serial.println("C");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setup(void)
{
  Serial.begin(115200);
  delay(1000);
  // Initialize the RTC
  setupRTC();

  // Initialize the NFC
  Serial.println("Hello!");
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata)
  {
    Serial.print("Didn't find PN53x board");
    while (1)
      ; // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5");
  Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. ");
  Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.');
  Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("Waiting for an ISO14443A Card ...");
  pinMode(LED_PIN, OUTPUT);
  ShowNotification();
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void loop(void)
{
  Serial.println("Waiting ...");
  uint8_t keya[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

  uint8_t success;
  uint8_t uid[16];   // Buffer to store the returned UID
  uint8_t uidLength; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success)
  {
    // Display some basic information about the card
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");
    Serial.print(uidLength, DEC);
    Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);
    Serial.println("");

    if (uidLength == 4)
    {
      indexToWriteFoxData = 5;
      uint8_t currentblockIndex; // Counter to keep track of which block we're on
      uint8_t data[16];          // Array to store block data during reads
      // Do not edit validBlocksForFoxes
      const uint8_t validBlocksForFoxes[NUM_PUNCHED + 1] =
          {
              4, 5, 6,    // Sector 1
              8, 9, 10,   // Sector 2
              12, 13, 14, // Sector 3
              16, 17, 18  // Sector 4
          };
      // We probably have a Mifare Classic card ...
      Serial.println("Seems to be a Mifare Classic card (4 byte UID)");

      // The following part is adapted from mifareclassic_memdump.ino
      // Now we try to go through all 16 sectors (each having 4 blocks)
      // authenticating each sector, and then dumping the blocks
      for (currentblockIndex = 0; currentblockIndex < sizeof(validBlocksForFoxes); currentblockIndex++)
      {
        success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, validBlocksForFoxes[currentblockIndex], 0, keya);
        if (!success)
        {
          // Try keyb
          Serial.println("keya failed, trying keyb on Block: ");
          Serial.print(validBlocksForFoxes[currentblockIndex], DEC);

          success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, validBlocksForFoxes[currentblockIndex], 1, keya);
        }

        if (success)
        {
// Write our test data here
#define BLOCK_LEN (16)
          uint8_t writeDataToCard[BLOCK_LEN];
          memset(writeDataToCard, 0x0, BLOCK_LEN * sizeof(uint8_t));
          if (validBlocksForFoxes[currentblockIndex] == 4)
          {
            writeDataToCard[0] = PUNCHER_ID;
            writeDataToCard[1] = indexToWriteFoxData;
          }
          else
          {
            memcpy(writeDataToCard, testDataFromFox[currentblockIndex - 1], DATA_FOX_HHMMSS * sizeof(uint8_t));
          }
          nfc.mifareclassic_WriteDataBlock(validBlocksForFoxes[currentblockIndex], writeDataToCard);

          // Authenticated ... we should be able to read the block now
          // Dump the data into the 'data' array
          success = nfc.mifareclassic_ReadDataBlock(validBlocksForFoxes[currentblockIndex], data);
          if (success)
          {
            // Read successful
            Serial.print("Block ");
            Serial.print(validBlocksForFoxes[currentblockIndex], DEC);
            if (validBlocksForFoxes[currentblockIndex] < 10)
            {
              Serial.print("  ");
            }
            else
            {
              Serial.print(" ");
            }
            // Dump the raw data
            nfc.PrintHexChar(data, 16);
          }
          else
          {
            // Oops ... something happened
            Serial.print("Block ");
            Serial.print(validBlocksForFoxes[currentblockIndex], DEC);
            Serial.println(" unable to read this block");
          }
        }
        else
        {
          Serial.println("Ooops ... authentication failed: Try another key?");
        }
      }
    }

    if (uidLength == 7)
    {
      // We probably have a Mifare Ultralight card ...
      Serial.println("Seems to be a Mifare Ultralight tag (7 byte UID)");
      Serial.println("Trying to authenticate block 4 with default KEYA value");
      success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4 + 0, 0, keya);

      // Try to read the first general-purpose user page (#4)
      Serial.println("Reading page 4");
      uint8_t data[SIZE_OF_DATA];
      for (uint8_t iPage = 0; iPage <= NUM_PUNCHED; iPage++)
      {
        uint8_t writePuncherID[SIZE_OF_DATA];
        memset(writePuncherID, 0x0, SIZE_OF_DATA * sizeof(uint8_t));
        if (iPage == 0)
        {
          writePuncherID[0] = PUNCHER_ID;
          writePuncherID[1] = indexToWriteFoxData;
        }
        else
        {
          memcpy(writePuncherID, testDataFromFox[iPage - 1], SIZE_OF_DATA * sizeof(uint8_t));
        }
        success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4 + iPage, 0, keya);
        success = nfc.mifareultralight_WritePage(4 + iPage, writePuncherID);
        if (!success)
        {
          Serial.println("Ooops ... unable to write the requested page!?");
          break;
        }
      }
      for (uint8_t iPage = 0; iPage <= NUM_PUNCHED; iPage++)
      {
        success = nfc.mifareultralight_ReadPage(4 + iPage, data);
        if (success)
        {
          // Data seems to have been read ... spit it out
          nfc.PrintHexChar(data, SIZE_OF_DATA);
          if (iPage == NUM_PUNCHED)
          {
            // Successfully cleared factor set the puncher
            ShowNotification();
          }
        }
        else
        {
          Serial.println("Ooops ... unable to read the requested page!?");
          break;
        }
      }
      RTCReadOut();
      // Wait a bit before reading the card again
      delay(1000);
    }
  }
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void ShowNotification(void)
{
  tone(BUZZ_PIN, 440);
  digitalWrite(LED_PIN, HIGH); // turn the LED on (HIGH is the voltage level)
  delay(500);                  // Wait a bit before reading the card again
  noTone(BUZZ_PIN);
  digitalWrite(LED_PIN, LOW);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime &dt)
{
  char datestring[20];

  snprintf_P(datestring,
             countof(datestring),
             PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
             dt.Month(),
             dt.Day(),
             dt.Year(),
             dt.Hour(),
             dt.Minute(),
             dt.Second());
  Serial.print(datestring);
}
