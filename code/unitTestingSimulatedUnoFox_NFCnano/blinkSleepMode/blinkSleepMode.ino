/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/
#include <avr/sleep.h>
#include <avr/power.h>

#define PN532_IRQ (2)
// the setup function runs once when you press reset or power the board
#define Sprint(a) (Serial.print(a))
#define Sprint2(a, x) (Serial.print(a, x))
#define Sprintln(a) (Serial.println(a))
#define Sprintln2(a, x) (Serial.println(a, x))

#define TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN (10000) // 10 second non-blocking delay

#define USE_TIMER_1 false
#define USE_TIMER_2 true
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false
#include "TimerInterrupt.h"

static void enableDetectionOrienteeringEnd()
{
  // Must clear any pending interrupts before using INT1
  // Otherwise, orienteer punching will glow the LED
  EIFR = (1 << INTF1);
  // Detach the timer
  ITimer2.detachInterrupt();
  Sprintln("Timer expired: Enabled orienteering end detection");
}

void sleepUntilCardComesInField()
{
  sleep_disable(); //disable sleep, awake now
  /*
  detachInterrupt(digitalPinToInterrupt(PN532_IRQ));
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
  */
}

static void printReg()
{
#if 1
  Serial.println((String) "PRR:" + PRR + "ADCSRA:" + ADCSRA + " ACSR:" + ACSR + " DIDR1:" + DIDR1 + " MCUCR:" + MCUCR);
#else
  Sprint("PRR:");
  Sprintln2(PRR, HEX);
  Sprint("ADCSRA:");
  Sprintln2(ADCSRA, HEX);
  Sprint("ACSR:");
  Sprintln2(ACSR, HEX);
  Sprint("DIDR1:");
  Sprintln2(DIDR1, HEX);
  Sprint("MCUCR:");
  Sprintln2(MCUCR, HEX);
#endif
  Serial.flush();
}

void setup()
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PN532_IRQ, INPUT_PULLUP); // PULL_UP otherwise floating values
  Serial.begin(115200);
  Sprint("compiled: ");
  Sprint(__DATE__);
  Sprintln(__TIME__);

  uint8_t arr0[3] = {0x7D, 0x22, 0xE2};
  uint8_t arr1[3] = {0x7D, 0x22, 0xE3};

  bool isEqual = !memcmp(arr0, arr1, sizeof(arr0));
  Sprint("Result (1 means equal, 0 means unequal)=");
  Sprintln(isEqual);

  ITimer2.init();

  if (ITimer2.attachInterruptInterval(TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN, enableDetectionOrienteeringEnd))
    Sprintln("Start  ITimer2 OK, millis() = " + String(millis()));
  else
    Sprintln("Can't set ITimer2. Select another freq., duration or timer");
}

// the loop function runs over and over again forever
void loop()
{
  Sprintln("Loop Start");
  digitalWrite(LED_BUILTIN, HIGH); // turn the LED on (HIGH is the voltage level)
  delay(1000);                     // wait for a second
  digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
  delay(1000);                     // wait for a second
  printReg();

  attachInterrupt(digitalPinToInterrupt(PN532_IRQ), sleepUntilCardComesInField, LOW);
  set_sleep_mode(SLEEP_MODE_PWR_SAVE);
  sleep_mode();

  Sprintln("After Sleep");
}
