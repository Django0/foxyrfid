/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

#include <avr/sleep.h>
#include <avr/power.h>

#define PIN_DETECT_PWM (3)                       // Pin to sample the PWM signal
#define TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN (1000L) // 10 second non-blocking delay

#define UNUSED_PIN_D9 (9)
#define UNUSED_PIN_D10 (10)
#define UNUSED_PIN_D11 (11)
#define UNUSED_PIN_D12 (12)
#define UNUSED_PIN_D13 (13)
#define UNUSED_PIN_A0 (A0)
#define UNUSED_PIN_A1 (A1)
#define UNUSED_PIN_A2 (A2)
#define UNUSED_PIN_A3 (A3)
#define UNUSED_PIN_A6 (A6)
#define UNUSED_PIN_A7 (A7)

#define USE_TIMER_1 true
#define USE_TIMER_2 false
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false
#include "TimerInterrupt.h"

void enableDetectionHuntEnd()
{
  // Clear any pending interrupts before using INT1
  EIFR = (1 << INTF1);
  ITimer1.detachInterrupt();
  Serial.println("Timer expired: Enabled hunt end detection");
  // Configure interrupt for the PIN_DETECT_PWM to go LOW (after Robert's inverting transistor) after the hunt.
  EIFR = (1 << INTF1);
  attachInterrupt(digitalPinToInterrupt(PIN_DETECT_PWM), shutdownNfcBoardISR, RISING);
}

void shutdownNfcBoardISR()
{
  // Cut power for NFC and RTC as the fox signals HIGH that the hunt has ended
  Serial.println("ISR called Hunt completed, setup power down");
  Serial.flush();
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW); // To measure the lowest power
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();

  MCUCR = MCUCR | bit(BODSE) | bit(BODS); // timed sequence
  MCUCR = MCUCR & (~bit(BODSE) | bit(BODS));
  Serial.print("MCUCR:");
  Serial.println(MCUCR, HEX);
  Serial.flush();
  sleep_cpu();
}
// the setup function runs once when you press reset or power the board
void setup()
{

  // Write the clock prescaler change enable (CLKPCE) bit to one and all other bits in CLKPR to zero.
  // Within four cycles, write the desired value to CLKPS while writing a zero to CLKPCE.
  CLKPR = 0x80;
  CLKPR = 0x00; // 0x00 = 16MHz
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  Serial.begin(115200);
  Serial.print("compiled: ");
  Serial.print(__DATE__);
  Serial.println(__TIME__);

#if 1
  Serial.print("PRR:");
  Serial.println(PRR, HEX);
  Serial.print("ADCSRB:");
  Serial.println(ADCSRB, HEX);
  Serial.print("ACSR:");
  Serial.println(ACSR, HEX);
  Serial.print("DIDR1:");
  Serial.println(DIDR1, HEX);
  power_adc_disable();
  power_spi_disable();
  power_timer2_disable();
  ADCSRB |= 0x40;
  ACSR |= 0x80;

  Serial.print("PRR:");
  Serial.println(PRR, HEX);
  Serial.print("ADCSRB:");
  Serial.println(ADCSRB, HEX);
  Serial.print("ACSR:");
  Serial.println(ACSR, HEX);
  Serial.print("DIDR1:");
  Serial.println(DIDR1, HEX);
  Serial.print("MCUCR:");
  Serial.println(MCUCR, HEX);

  Serial.flush();
#endif

#if 1
  // Orienteering=27.6mA, Shutdown=12.9mA
  pinMode(UNUSED_PIN_D9, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D10, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D11, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D12, INPUT_PULLUP);
  pinMode(UNUSED_PIN_D13, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A0, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A1, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A2, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A3, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A6, INPUT_PULLUP);
  pinMode(UNUSED_PIN_A7, INPUT_PULLUP);
#else
  // Orienteering=28.5mA, Shutdown=12.8mA
  uint8_t unUsedPins[] = {
      9, 10, 11, 12, 13,     // Digital Pins: D9 to D13
      18, 19, 20, 21, 24, 25 // Analog Pins: A0 to A3 and A6, A7
  };

  for (uint8_t iPinIndex = 0; iPinIndex < sizeof(unUsedPins); iPinIndex++)
  {
    pinMode(unUsedPins[iPinIndex], INPUT_PULLUP);
  }
#endif
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_DETECT_PWM, INPUT_PULLUP); // Has to be a pullup otherwise we might burn the GPIO or the transistor

  unsigned long duration;
  while (1)
  {
    duration = pulseInLong(PIN_DETECT_PWM, HIGH, 4e6); // 4 seconds
    Serial.print("LED High duration (expected 1 second) = ");
    Serial.println(duration);
    Serial.flush();
    if (duration == 0)
    {
      // HIGH signal not detected for 4 seconds
      // the fox has started morse transmission, so power up NFC and RTC
      //digitalWrite(pinTurnOnHiSideSwitch, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
      ITimer1.init();

      if (ITimer1.attachInterruptInterval(TIMEOUT_TO_ENABLE_IRQ_POWER_DOWN, enableDetectionHuntEnd))
        Serial.println("Starting  ITimer1 OK, millis() = " + String(millis()));
      else
        Serial.println("Can't set ITimer1. Select another freq., duration or timer");
      break;
    }
    else
    {
      digitalWrite(LED_BUILTIN, HIGH); // turn the LED on (HIGH is the voltage level)
      delay(100);
      digitalWrite(LED_BUILTIN, LOW); // turn the LED on (HIGH is the voltage level)
    }
  }
}

// the loop function runs over and over again forever
void loop()
{
  Serial.println("Infinite main loop");
  Serial.flush();
  delay(100);
}
