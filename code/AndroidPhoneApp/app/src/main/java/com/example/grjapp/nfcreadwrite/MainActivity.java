package com.example.grjapp.nfcreadwrite;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.nfc.tech.MifareClassic;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Vibrator ;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import android.os.VibrationEffect;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity {

    public static final String ERROR_DETECTED = "No NFC tag detected!";
    public static final String WRITE_SUCCESS = "Erased NFC tag successfully!";
    public static final String WRITE_ERROR = "Error during writing, is the NFC tag close enough to your device?";
    public static final byte[] validBlocksForFoxesMifareClassic =
                                                            {                      // In Hex
                                                                    0x04,0x05,0x06,    // Sector 1
                                                                    0x08,0x09,0x0A,    // Sector 2
                                                                    0X0C,0x0D,0x0E,    // Sector 3
                                                                    0x10,0x11,0x12     // Sector 4
                                                            };

    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    boolean writeMode;
    Tag myTag;
    Context context;
    boolean eraseTagFlag = false;

    TextView showBuildDate;
    TextView puncherId;
    TextView fox;

    TextView message;
    Button btnWrite0;
    Button btnWrite1;

    ImageView image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        shakeItBaby(500);

        showBuildDate = (TextView) findViewById(R.id.showBuildDate);
        puncherId = (TextView) findViewById(R.id.puncherId);

        fox = (TextView) findViewById(R.id.fox);

        //message = (TextView) findViewById(R.id.edit_message);
        btnWrite0 = (Button) findViewById(R.id.button0);
        btnWrite1 = (Button) findViewById(R.id.button1);

        //image = (ImageView) findViewById(R.id.imageView);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date buildDate = new Date(Long.parseLong(BuildConfig.BUILD_TIME));
        showBuildDate.setText(String.format("Built:%s ", formatter.format(buildDate)));
        Log.i("MyProgram", "This .apk was built on " + buildDate.toString());

        btnWrite0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eraseTagFlag = !eraseTagFlag;
                setButtonColor();
            }

        });

        btnWrite1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eraseTagFlag == true) {
                    try {
                        if (myTag == null) {
                            Toast.makeText(context, ERROR_DETECTED, Toast.LENGTH_LONG).show();
                        } else {
                            write(null, myTag);
                            //Toast.makeText(context, WRITE_SUCCESS, Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        Toast.makeText(context, WRITE_ERROR, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    } catch (FormatException e) {
                        Toast.makeText(context, WRITE_ERROR, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
                eraseTagFlag = false;
                setButtonColor();
            }
        });

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
        } else if (!nfcAdapter.isEnabled()) {
            // NFC is available for device but not enabled
            Toast toast = Toast.makeText(this, "Please enable NFC in the settings", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        } else {
            // NFC is enabled, continue as normal
        }
        readFromIntent(getIntent());

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };
    }

    /******************************************************************************
     **********************************Vibrate***************************
     ******************************************************************************/
    // Vibrate for  milliseconds
    private void shakeItBaby(long timeMs) {
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(timeMs,128));
        }
    }
    /******************************************************************************
     **********************************Set button color***************************
     ******************************************************************************/

    private void setButtonColor() {
        if (eraseTagFlag) {
            btnWrite0.setBackgroundColor(0xffff0000);   // Red and ready to erase
        } else {
            btnWrite0.setBackgroundColor(0xff888888);   // Grey
        }
    }

    /******************************************************************************
     **********************************Read From NFC Tag***************************
     ******************************************************************************/
    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = myTag.getTechList();
            NfcA nfcA = NfcA.get(myTag);
            MifareClassic mfC = MifareClassic.get(myTag);;

            int len = nfcA.getMaxTransceiveLength();
            int firstBlockNum = 0;
            int lastBlockNum = 42;
            byte[] result = {0};
            byte[][] resultMifareClassic = new byte[12][16];
            /*
            final byte[] validBlocksForFoxesMifareClassic =
                                            {                      // In Hex
                                                0x04,0x05,0x06,    // Sector 1
                                                0x08,0x09,0x0A,    // Sector 2
                                                0X0C,0x0D,0x0E,    // Sector 3
                                                0x10,0x11,0x12     // Sector 4
                                            };

             */
            try {
                if (mfC == null) {
                    nfcA.connect();
                    // Following only works for ultralight
                    result = nfcA.transceive(new byte[]{
                            (byte) 0x3A,  // FAST_READ
                            (byte) (firstBlockNum & 0x0ff),
                            (byte) (lastBlockNum & 0x0ff),
                    });
                } else {
                    // Mifare Classic
                    mfC.connect();
                    byte [] keyA = {(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
                    boolean flagIsAuthNeeded = false;
                    for (int index = 0; index < resultMifareClassic.length; index++) {
                        if (validBlocksForFoxesMifareClassic[index] < 128) {
                            flagIsAuthNeeded =( (validBlocksForFoxesMifareClassic[index] % 4) == 0);
                        } else {
                            flagIsAuthNeeded = (validBlocksForFoxesMifareClassic[index] % 16 == 0);
                        }
                        if (flagIsAuthNeeded == true) {
                            mfC.authenticateSectorWithKeyA(validBlocksForFoxesMifareClassic[index]/4,keyA);     // Sector x
                        }
                        resultMifareClassic[index] = mfC.readBlock(validBlocksForFoxesMifareClassic[index]);
                    }
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException while reading tag...", e);
            } finally {
                shakeItBaby(200);
                if (fox.getEditableText() != null) {
                    fox.getEditableText().clear();
                }
                if (mfC == null) {
                    // Following is specific for the ultralight sticker card
                    puncherId.setText(String.format("Puncher ID:%s", result[16]));

                    int countFoxes = 0;
                    for (int index = 20; index <= 100; index = index + 4, countFoxes++) {
                        if ((countFoxes <= 10) && (result[index] != 125)) { // Skip the repeated punched fox
                            fox.append(String.format("%2s # %2s:%2s:%2s\n",
                                    result[index], result[index + 1], result[index + 2], result[index + 3]));
                        }
                    }
                } else {
                    // Mifare Classic card
                    puncherId.setText(String.format("Puncher ID:%s", resultMifareClassic[0][0]));
                    for (int index = 1; index < resultMifareClassic.length; index++) {
                        fox.append(String.format("%2s # %2s:%2s:%2s\n",
                                resultMifareClassic[index][0],
                                resultMifareClassic[index][1],
                                resultMifareClassic[index][2],
                                resultMifareClassic[index][3]));
                    }
                }
                if (nfcA != null) {
                    try {
                        nfcA.close();
                    } catch (IOException e) {
                        Log.e(TAG, "Error closing tag...", e);
                    }
                }
                if (mfC != null) {
                    try {
                        mfC.close();
                    }
                    catch (IOException e) {
                        Log.e(TAG, "Error closing tag...", e);
                    }
                }

            }
        }
    }
    private void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        try {
            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }

        //foxContent.setText("NFC Content: " + text);
    }


    /******************************************************************************
     **********************************Write to NFC Tag****************************
     ******************************************************************************/
    private void write(String text, Tag tag) throws IOException, FormatException {
// TODO: ERASE pending for Mifare Classic card
        NfcA nfcA = NfcA.get(tag);
        MifareClassic mfC = MifareClassic.get(tag);

        byte[] result = {0};
        try {
            if (mfC == null) {
                nfcA.connect();
                byte[] puncherIdPage = {0};
                puncherIdPage = nfcA.transceive(new byte[]{
                        (byte) 0x3A,  // FAST_READ
                        (byte) (0x4 & 0x0ff),
                        (byte) (0x4 & 0x0ff),
                });
                for (int pageNum = 4; pageNum <= 0x0F; pageNum++) {
                    if (pageNum != 4) {
                        result = nfcA.transceive(new byte[]{
                                (byte) 0xA2,  // WRITE
                                (byte) (pageNum & 0x0ff),
                                0, 0, 0, 0
                        });
                    } else {
                        result = nfcA.transceive(new byte[]{
                                (byte) 0xA2,  // WRITE
                                (byte) (pageNum & 0x0ff),
                                puncherIdPage[0], 1, 0, 0         // puncherId, indexToStoreFox
                        });
                    }
                }
                Toast.makeText(context, WRITE_SUCCESS, Toast.LENGTH_LONG).show();
            } else {
                // Mifare Classic
                byte [] keyA = {(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
                byte[] miFareClassicBlock = new byte [16];
                boolean flagIsAuthNeeded = false;

                mfC.connect();
                for (int index = 0; index < 12; index++) {
                    if (validBlocksForFoxesMifareClassic[index] < 128) {
                        flagIsAuthNeeded =( (validBlocksForFoxesMifareClassic[index] % 4) == 0);
                    } else {
                        flagIsAuthNeeded = (validBlocksForFoxesMifareClassic[index] % 16 == 0);
                    }
                    if (flagIsAuthNeeded == true) {
                        mfC.authenticateSectorWithKeyA(validBlocksForFoxesMifareClassic[index]/4,keyA);     // Sector x
                    }
                    if (validBlocksForFoxesMifareClassic[index] == 0x04) {
                        // Save puncherId, which is in miFareClassicBlock[0]
                        miFareClassicBlock = mfC.readBlock(validBlocksForFoxesMifareClassic[index]);
                        miFareClassicBlock[1] = 0x05; // Erase absIndex
                    } else {
                        // Clear all fox timestamps
                        Arrays.fill(miFareClassicBlock, (byte) 0x0);
                    }
                    // Perform erase
                    mfC.writeBlock(validBlocksForFoxesMifareClassic[index],miFareClassicBlock);
                }

                Toast.makeText(context, WRITE_SUCCESS, Toast.LENGTH_LONG).show();
            }

        } catch (IOException e) {
            Log.e(TAG, "IOException while reading tag...", e);
            Toast.makeText(context, WRITE_ERROR, Toast.LENGTH_LONG).show();
        } finally {
            if (nfcA != null) {
                try {
                        nfcA.close();
                    }
                catch (IOException e) {
                    Log.e(TAG, "Error closing tag...", e);
                }
            }
            if (mfC != null) {
                try {
                    mfC.close();
                }
                catch (IOException e) {
                    Log.e(TAG, "Error closing tag...", e);
                }
            }
        }
    }
    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang       = "en";
        byte[] textBytes  = text.getBytes();
        byte[] langBytes  = lang.getBytes("US-ASCII");
        int    langLength = langBytes.length;
        int    textLength = textBytes.length;
        byte[] payload    = new byte[1 + langLength + textLength];

        // set status byte (see NDEF spec for actual bits)
        payload[0] = (byte) langLength;

        // copy langbytes and textbytes into payload
        System.arraycopy(langBytes, 0, payload, 1,              langLength);
        System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

        NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,  NdefRecord.RTD_TEXT,  new byte[0], payload);

        return recordNFC;
    }



    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        readFromIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }

    @Override
    public void onResume(){
        super.onResume();
        WriteModeOn();
    }



    /******************************************************************************
     **********************************Enable Write********************************
     ******************************************************************************/
    private void WriteModeOn(){
        writeMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }
    /******************************************************************************
     **********************************Disable Write*******************************
     ******************************************************************************/
    private void WriteModeOff(){
        writeMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }
}